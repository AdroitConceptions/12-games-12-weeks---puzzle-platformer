﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandOnButton : MonoBehaviour {

	Rigidbody2D rb2d;

	public MovingPlatform movingPlatform;
	PlayerPlatformerController player;
	bool down = false;
	bool moving = false;
	Vector2 defaultPos;

	// Use this for initialization
	void Start () {
		rb2d = GetComponent<Rigidbody2D> ();
		defaultPos = transform.position;

	}

	void FixedUpdate () {
		if (!moving) return;

		Vector2 target = defaultPos;
		if (down) {
			target += new Vector2 (0f, -.2f);
		}

		if (Vector2.Distance (rb2d.position, target) < 0.01f) {
			moving = false;
			return;
		}

		Vector2 move = (target - rb2d.position).normalized * 5f * Time.deltaTime;
		rb2d.position = rb2d.position + move;
		transform.position = rb2d.position;

		if (player) {
			player.Move (move);
		}

	}

	void OnCollisionEnter2D (Collision2D col) {
		Debug.Log ("here");
		PlayerPlatformerController _player = col.gameObject.GetComponent<PlayerPlatformerController> ();
		if (_player) {
			player = _player;
			moving = true;
			down = true;
			movingPlatform.on = true;
		}
	}

	void OnCollisionExit2D (Collision2D col) {
		PlayerPlatformerController _player = col.gameObject.GetComponent<PlayerPlatformerController> ();
		if (_player) {
			player = null;
			moving = true;
			down = false;
		}
	}
}