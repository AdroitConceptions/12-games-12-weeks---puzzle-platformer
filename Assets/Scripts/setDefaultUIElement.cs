﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class setDefaultUIElement : MonoBehaviour {

	public GameObject defaultElement;
	private GameObject priorElement;

	void OnEnable () {
		priorElement = EventSystem.current.gameObject;
		StartCoroutine (set_default_element ());
	}
	IEnumerator set_default_element () {
		yield return null;
		if (defaultElement != null)
			EventSystem.current.SetSelectedGameObject (defaultElement);

	}

	void OnDisable () {
		if (priorElement != null)
			EventSystem.current.SetSelectedGameObject (priorElement);
	}
}