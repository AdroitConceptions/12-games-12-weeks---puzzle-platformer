﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour {

	public GameObject target;

	// void LateUpdate () {
	// 	Vector3 CameraTarget = transform.position;
	// 	CameraTarget.x = target.transform.position.x;

	// 	transform.position = CameraTarget;
	// }

	void FixedUpdate () {
		if (target == null) return;

		Vector3 CameraTarget = transform.position;
		CameraTarget.x = target.transform.position.x;

		transform.position = Vector3.Lerp (transform.position, CameraTarget, 0.1f);
	}
}