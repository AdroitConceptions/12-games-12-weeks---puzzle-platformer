﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinPickupScript : MonoBehaviour {

	public GameObject PickupBurst;

	void OnTriggerEnter2D (Collider2D col) {
		if (col.CompareTag ("Player")) {
			EventDirector.coinPickup ();
			Instantiate (PickupBurst, transform.position, Quaternion.identity);
			Destroy (gameObject);
		}
	}
}