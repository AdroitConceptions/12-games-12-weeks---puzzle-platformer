﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameStateManager : MonoBehaviour {

	[Header ("UI Elements")]
	public Text scoreBox;
	public GameObject pauseMenu;
	public GameObject LevelOverScreen;
	public Text endScore;
	public GameObject burst;

	private int score = 0;
	private bool paused = false;
	private bool endState = false;

	void Start () {
		setPauseState (false);
		LevelOverScreen.SetActive (false);
	}

	void Update () {
		if (!endState && InputMapper.GetKeyDown (InputMapper.ActionCode.pause)) {
			setPauseState (!paused);
		}
	}

	public void UnPause () {
		setPauseState (false);
	}

	public void ReStartLevel () {
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
	}

	public void QuitGame () {
		Debug.Log ("Quit Game");
		Application.Quit ();
	}

	void setPauseState (bool _paused) {
		paused = _paused;
		Time.timeScale = _paused ? 0f : 1f;
		pauseMenu.SetActive (_paused);
	}

	void OnEnable () {
		EventDirector.coinPickup += OnCoinPickup;
		EventDirector.levelEnd += OnLevelEnd;
	}

	void OnDisable () {
		EventDirector.coinPickup -= OnCoinPickup;
		EventDirector.levelEnd -= OnLevelEnd;
	}

	void OnCoinPickup () {
		score += 100;
		scoreBox.text = score.ToString ();
	}

	void OnLevelEnd (bool success) {
		endState = true;
		Time.timeScale = 0f;
		LevelOverScreen.SetActive (true);
		endScore.text = success ? "Final Score: " + score.ToString () : "You Died!";
		if (success) StartCoroutine (FinishBurst ());
	}

	IEnumerator FinishBurst () {
		Vector3 center = Camera.main.gameObject.transform.position;
		center.z = 0;

		for (int i = 0; i < 10; i++) {
			Vector2 offset = Random.insideUnitCircle * 3f;
			Instantiate (burst, center + (Vector3) offset, Quaternion.identity);
			yield return new WaitForSecondsRealtime (0.1f);
		}
	}
}