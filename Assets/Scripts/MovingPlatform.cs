﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour {

	Rigidbody2D rb2d;

	public Vector2[] waypoints;
	bool loop = true;
	int currentWaypoint = 0;
	bool posDirection = true;
	public float speed = 1;
	public bool on = true;

	PlayerPlatformerController player;

	// Use this for initialization
	void Start () {
		rb2d = GetComponent<Rigidbody2D> ();

	}

	void FixedUpdate () {
		if (!on) return;
		if (Vector2.Distance (waypoints[currentWaypoint], rb2d.position) < 0.1f) {
			currentWaypoint += posDirection ? 1 : -1;
			if (currentWaypoint < 0 || currentWaypoint >= waypoints.Length) {
				if (loop) {
					currentWaypoint %= waypoints.Length;
				} else {
					posDirection = !posDirection;

					currentWaypoint = posDirection ? waypoints.Length - 1 : 0;
				}
			}
		}

		Vector2 move = (waypoints[currentWaypoint] - rb2d.position).normalized * speed * Time.deltaTime;
		rb2d.position = rb2d.position + move;
		transform.position = rb2d.position;

		if (player) {
			player.Move (move);
		}

	}

	void OnCollisionEnter2D (Collision2D col) {
		Debug.Log ("here");
		PlayerPlatformerController _player = col.gameObject.GetComponent<PlayerPlatformerController> ();
		if (_player) {
			player = _player;
		}
	}

	void OnCollisionExit2D (Collision2D col) {
		PlayerPlatformerController _player = col.gameObject.GetComponent<PlayerPlatformerController> ();
		if (_player) {
			player = null;
		}
	}
}