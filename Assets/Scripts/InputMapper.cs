﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputMapper {
	#region OS Joystick Mapping Directives
#if UNITY_STANDALONE_WIN
	const string JoyLeftX = "JHorizontal";
	const string JoyLeftY = "JVertical";
	const string JoyRightX = "Joy4";
	const string JoyRightY = "Joy5";
	const string JoyRT = "Joy10";
	const string JoyLT = "Joy9";
	const string JoyDX = "joy6";
	const string JoyDY = "joy7";

	const string JoyButtonA = "joystick button 0";
	const string JoyButtonB = "joystick button 1";
	const string JoyButtonX = "joystick button 2";
	const string JoyButtonY = "joystick button 3";
	const string JoyButtonLB = "joystick button 4";
	const string JoyButtonRB = "joystick button 5";
	const string JoyButtonBack = "joystick button 6";
	const string JoyButtonStart = "joystick button 7";
	const string JoyButtonClickL = "joystick button 8";
	const string JoyButtonClickR = "joystick button 9";

	const string JoyDLeft = null;
	const string JoyDRight = null;
	const string JoyDUp = null;
	const string JoyDDown = null;

#elif UNITY_STANDALONE_OSX
	const string JoyLeftX = "JHorizontal";
	const string JoyLeftY = "JVertical";
	const string JoyRightX = "Joy3";
	const string JoyRightY = "Joy4";
	const string JoyRT = "Joy6";
	const string JoyLT = "Joy5";
	const string JoyDX = null;
	const string JoyDY = null;

	const string JoyButtonA = "joystick button 16";
	const string JoyButtonB = "joystick button 17";
	const string JoyButtonX = "joystick button 18";
	const string JoyButtonY = "joystick button 19";
	const string JoyButtonLB = "joystick button 13";
	const string JoyButtonRB = "joystick button 14";
	const string JoyButtonBack = "joystick button 10";
	const string JoyButtonStart = "joystick button 9";
	const string JoyButtonClickL = "joystick button 11";
	const string JoyButtonClickR = "joystick button 12";

	const string JoyDLeft = "joystick button 5";
	const string JoyDRight = "joystick button 6";
	const string JoyDUp = "joystick button 7";
	const string JoyDDown = "joystick button 8";

#elif UNITY_EDITOR_LINUX || UNITY_STANDALONE_LINUX
	const string JoyLeftX = "JHorizontal";
	const string JoyLeftY = "JVertical";
	const string JoyRightX = "Joy4";
	const string JoyRightY = "Joy5";
	const string JoyRT = "Joy6";
	const string JoyLT = "Joy3";
	const string JoyDX = "joy7";
	const string JoyDY = "joy8";

	const string JoyButtonA = "joystick button 0";
	const string JoyButtonB = "joystick button 1";
	const string JoyButtonX = "joystick button 2";
	const string JoyButtonY = "joystick button 3";
	const string JoyButtonLB = "joystick button 4";
	const string JoyButtonRB = "joystick button 5";
	const string JoyButtonBack = "joystick button 6";
	const string JoyButtonStart = "joystick button 7";
	const string JoyButtonClickL = "joystick button 9";
	const string JoyButtonClickR = "joystick button 10";

	const string JoyDLeft = "joystick button 11";
	const string JoyDRight = "joystick button 12";
	const string JoyDUp = "joystick button 13";
	const string JoyDDown = "joystick button 14";

#endif
	#endregion

	public enum ActionCode {
		dash,
		fire,
		weaponCycle,
		pause,
		jump
	}

	private static bool joyStick = false;

	public static Vector2 getMove () {
		Vector2 inputKeyboard = new Vector2 (Input.GetAxisRaw ("KHorizontal"), Input.GetAxisRaw ("KVertical"));
		Vector2 inputJoystick = new Vector2 (Input.GetAxisRaw (JoyLeftX), Input.GetAxisRaw (JoyLeftY));

		if (joyStick && inputKeyboard.sqrMagnitude > 0.1f) {
			joyStick = false;
		} else if (!joyStick && inputJoystick.sqrMagnitude > 0.1f) {
			joyStick = true;
		}

		return joyStick ? inputJoystick : inputKeyboard;
	}

	public static bool GetKeyDown (ActionCode actionCode) {
		switch (actionCode) {
			case ActionCode.dash:
				return joyStick ? Input.GetKeyDown (JoyButtonLB) : Input.GetKeyDown (KeyCode.Space);
			case ActionCode.fire:
				return false;
			case ActionCode.weaponCycle:
				return Input.GetKeyDown (KeyCode.Q) || Input.GetKeyDown (JoyButtonRB);
			case ActionCode.pause:
				return Input.GetKeyDown (KeyCode.Escape) || Input.GetKeyDown (JoyButtonStart);
			case ActionCode.jump:
				return Input.GetKeyDown (KeyCode.Space) || Input.GetKeyDown (JoyButtonA);
			default:
				return false;
		}
	}

	public static bool GetKeyUp (ActionCode actionCode) {
		switch (actionCode) {
			case ActionCode.dash:
				return joyStick ? Input.GetKeyUp (JoyButtonLB) : Input.GetKeyUp (KeyCode.Space);
			case ActionCode.fire:
				return false;
			case ActionCode.weaponCycle:
				return Input.GetKeyUp (KeyCode.Q) || Input.GetKeyDown (JoyButtonRB);
			case ActionCode.pause:
				return Input.GetKeyUp (KeyCode.Escape) || Input.GetKeyUp (JoyButtonStart);
			case ActionCode.jump:
				return Input.GetKeyUp (KeyCode.Space) || Input.GetKeyUp (JoyButtonA);
			default:
				return false;
		}
	}

	public static bool GetKey (ActionCode actionCode) {
		switch (actionCode) {
			case ActionCode.dash:
				return joyStick ? Input.GetKey (JoyButtonLB) : Input.GetKey (KeyCode.Space);
			case ActionCode.fire:
				return joyStick ? Input.GetAxisRaw (JoyRT) > 0.1f : Input.GetKey (KeyCode.Mouse0);
			case ActionCode.weaponCycle:
				return Input.GetKey (KeyCode.Q) || Input.GetKey (JoyButtonRB);
			case ActionCode.pause:
				return Input.GetKey (KeyCode.Escape) || Input.GetKey (JoyButtonStart);
			case ActionCode.jump:
				return Input.GetKey (KeyCode.Space) || Input.GetKey (JoyButtonA);
			default:
				return false;
		}
	}
}