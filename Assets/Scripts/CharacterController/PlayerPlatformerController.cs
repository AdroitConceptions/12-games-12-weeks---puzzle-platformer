﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (AudioSource))]
public class PlayerPlatformerController : PhysicsObject {

	public float maxSpeed = 7;
	public float jumpTakeOffSpeed = 7;
	public AudioClip jump;
	public AudioClip hurt;

	private SpriteRenderer spriteRenderer;
	//private Animator animator;
	private AudioSource audioSource;

	// Use this for initialization
	void Awake () {
		spriteRenderer = GetComponentInChildren<SpriteRenderer> ();
		//animator = GetComponentInChildren<Animator> ();
		audioSource = GetComponent<AudioSource> ();
	}

	public void TakeDamage () {
		Die ();
	}

	protected override void ComputeVelocity () {
		Vector2 move = Vector2.zero;

		move.x = InputMapper.getMove ().x;

		if (InputMapper.GetKeyDown (InputMapper.ActionCode.jump) && grounded) {
			velocity.y = jumpTakeOffSpeed;
			audioSource.PlayOneShot (jump);
		} else if (InputMapper.GetKeyUp (InputMapper.ActionCode.jump)) {
			if (velocity.y > 0) {
				velocity.y = velocity.y * 0.5f;
			}
		}

		bool flipSprite = (spriteRenderer.flipX ? (move.x > 0.01f) : (move.x < -0.01f));
		if (flipSprite) {
			spriteRenderer.flipX = !spriteRenderer.flipX;
		}

		//animator.SetBool ("grounded", grounded);
		//animator.SetFloat ("velocityX", Mathf.Abs (velocity.x) / maxSpeed);

		targetVelocity = move * maxSpeed;

		if (transform.position.y < -7f) {
			Die ();
		}
	}

	void Die () {
		audioSource.PlayOneShot (hurt);
		EventDirector.levelEnd (false);
	}
}