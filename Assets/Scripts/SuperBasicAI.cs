﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuperBasicAI : PhysicsObject {

	public float maxSpeed = 7;
	bool goingRight = false;
	public LayerMask ground;

	protected override void ComputeVelocity () {
		Vector2 move = Vector2.zero;
		Vector3 raycastOffset = Vector3.zero;
		raycastOffset.x = goingRight ? 0.6f : -0.6f;
		RaycastHit2D hit1 = Physics2D.Raycast (transform.position + raycastOffset, -Vector2.up, 1f, ground);
		RaycastHit2D hit2 = Physics2D.Raycast (transform.position + raycastOffset, Vector2.right * (goingRight ? 1f : -1f), .1f, ground);

		//Debug.Log (hit.collider.gameObject);
		if (hit1.collider == null || hit2.collider != null) {
			goingRight = !goingRight;
		}
		move.x = (goingRight ? 1f : -1f);

		targetVelocity = move * maxSpeed;
	}

	void OnCollisionEnter2D (Collision2D col) {
		PlayerPlatformerController player = col.gameObject.GetComponent<PlayerPlatformerController> ();
		if (player) {
			player.TakeDamage ();
		}

	}
}