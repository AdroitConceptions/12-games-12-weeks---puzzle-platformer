﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventDirector {

	public delegate void OnCoinPickup ();
	public static OnCoinPickup coinPickup;

	public delegate void LevelEnd (bool sucess);
	public static LevelEnd levelEnd;

}