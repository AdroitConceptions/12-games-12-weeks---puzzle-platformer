﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ScaleButton : MonoBehaviour, ISelectHandler, IDeselectHandler {

	//Vector2 offMax;
	//Vector2 offMin;
	RectTransform myRT;
	Vector2 sizeDelta;

	// Use this for initialization
	void Start () {
		myRT = transform as RectTransform;
		sizeDelta = myRT.sizeDelta;
		//offMax = myRT.offsetMax;
		//offMin = myRT.offsetMin;
	}

	public void OnSelect (BaseEventData data) {
		myRT.sizeDelta = sizeDelta * 1.1f;

		Debug.Log (gameObject.name + " Selected");
	}
	public void OnDeselect (BaseEventData data) {
		myRT.sizeDelta = sizeDelta;
		Debug.Log (gameObject.name + " Deselected");
	}
}